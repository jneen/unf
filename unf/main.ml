open Batteries
open Batteries.Printf
open Elron
open Syntax

let main () = printf ">> ";
  let decoder = Uutf.decoder (`Channel Legacy.stdin)
  in let rec stream () = flush stdout ; match Uutf.decode decoder with
  | `Await       -> stream ()
  | `End         -> Eof
  | `Malformed s -> raise (Failure "todo: handle malformed input") (* TODO *)
  | `Uchar i     -> Tok (UChar.of_int i)

  (* in let line_stream () = match stream () with
  | Tok t when UChar.eq t (UChar.of_char '\n') -> Eof
  | reply -> reply *)

  in try
    printf "parsing...\n";
    let syntax = Syntax.parse stream
    in printf "evaluating...\n"; let value = Eval.eval Eval.prelude syntax
    in printf "=> %s\n" (Eval.string_of_value value)
  with
    | ParseError (idx, expected) ->
        printf "expected one of %s at %i\n" (String.join ", " expected) idx
    | Eval.TypeError (exp, act) -> printf "type error: expected %s, got %s\n" exp act
    | Eval.VariableError s -> printf "variable error: %s\n" s

;;

let () = main () ;;
