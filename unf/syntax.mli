open Batteries
open Batteries.Printf
open Elron
open Elron.Unicode
open Expr

type t = [
  | `Expr of plain_syntax
  | `Decl of plain_decl
  | `Binder of binder
  | `Module of plain_decl list
]

val to_string : t -> string

val parse : (unit -> uchar_token) -> t
