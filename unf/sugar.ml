open Batteries
open Batteries.Printf
open Expr

let rec desugar : plain_syntax -> plain_expr = function
  | `Autovar -> `Var "$"
  | `Var x -> `Var x
  | `Tag x -> `Tag x
  | `Int n -> `Int n
  | `Lam cs ->
      let unicase clauses = `Unilam ("#d", `Case (`Var "#d", clauses))
      in let rec simplify : (binder list * plain_syntax) -> binder * plain_expr = function
        | [], _ -> raise (Failure "clause with no bindings") (* unreachable *)
        | [b], e -> b, desugar e
        | (b::bs), e -> b, unicase [simplify (bs, e)]
      in unicase (List.map simplify cs)
  | `Autolam e -> `Unilam ("$", desugar e)
  | `Let (decls, expr) -> `Let (decls |> List.map desugar_decl, desugar expr)
  | `Apply (f, arg) -> `Apply (desugar f, desugar arg)
  | `Chain (arg, f) -> `Apply (desugar f, desugar arg)
  | `Lazy e -> `Lazy (desugar e)

and desugar_decl : plain_decl -> plain_expr_decl = function
  | `Method (n, bs) -> `Method (n, bs)
  | `Impl (n, bs, e) -> `Impl (n, bs, desugar e)
  | `Func (n, bs, e) -> `Func (n, bs, desugar e)

let rec bound_vars : binder -> string Set.t = function
  | `Var x -> Set.singleton x
  | `Tagged (_, bs) -> multi_bound_vars bs

and multi_bound_vars bs = bs |> List.map bound_vars |> List.fold_left Set.union Set.empty
