open Batteries
open Batteries.Printf
open Expr

(*
 * string set expr_with_ann
 * = (string set * string set expr_with_annn) a_expr
 * k
 *)

type 'a expr_with_ann = ('a * 'a expr_with_ann) a_expr
type 'a ann_expr = 'a * 'a expr_with_ann

type free_ann = string Set.t ann_expr

let free_in : free_ann -> string Set.t = function (vs, _) -> vs

let rec annotate (f:'a expr_with_ann -> 'a) (e:'b a_expr) : 'a ann_expr =
  let result : 'a expr_with_ann = map_subexprs (annotate f) e
  in (f result, result)

let rec annotate_free (e:plain_expr) : free_ann =
  let annotate_fn : string Set.t expr_with_ann -> string Set.t = function
    | `Var x -> Set.singleton x
    | `Case (d, cases) ->
      cases |> List.map (fun (_, e) -> free_in e)
            |> List.fold_left Set.union (free_in d)
    | `Unilam (a, b) -> free_in b |> Set.remove a
    | `Lazy e -> free_in e
    | `Let (decls, e) ->
        let bound = function
          | `Method (s, _) -> Set.singleton s
          | `Impl (_, _, _) -> Set.empty
          | `Func (s, _, _) -> Set.singleton s

        in let bound_vars = decls |> List.map bound |> List.fold_left Set.union Set.empty
        in let decl_free = decls |> List.map Expr.decl_subexprs |> List.flatten
                                 |> List.map free_in
                                 |> List.fold_left Set.union Set.empty
        in Set.diff decl_free bound_vars
    | `Apply (f, arg) -> Set.union (free_in f) (free_in arg)
    | _ -> Set.empty
  in annotate annotate_fn e
