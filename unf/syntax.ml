open Batteries
open Batteries.Printf
open Elron
open Elron.Unicode
open Expr

type t = [
  | `Expr of plain_syntax
  | `Decl of plain_decl
  | `Binder of binder
  | `Module of plain_decl list
]

(* TODO *)
let decl_to_string _ = ""

let to_string = function
  | `Expr e -> plain_syntax_to_string e
  | `Decl d -> plain_decl_to_string d
  | `Binder b -> binder_to_string b
  | `Module ds -> plain_decls_to_string ds

let debug s p = p |> map (fun x -> printf "debug: %s\n" s; x)

let spaces = alt [ ch ' '; ch '\t' ] |> many |> ignore
let lexeme p = p |> skip spaces

let nl      = alt [ ch '\n'; ch '\r' |> follow (ch '\n') ] |> ignore
let comment = ch '#' |> follow (many (noneOf "\r\n")) |> follow (opt nl) |> ignore
let lines = alt [ nl; comment; ch ';' |> ignore ] |> follow spaces |> many |> ignore
let lineme p = p |> skip spaces |> skip lines

let digit = range '0' '9'
let id_start = alt [range 'a' 'z'; range 'A' 'Z'; ch '_']
let id_body = alt [id_start; digit; ch '-']
let id = debug "id_start" id_start |> seq (many id_body)
                  |> map (fun (x, xs) -> Elron.Unicode.string_of_uchars (x::xs))
                  |> desc "an identifier"

let tagged = ch '.' |> follow id |> lexeme
let numeral = many1 digit |> lexeme
let lbrace = ch '[' |> lineme
let rbrace = ch ']' |> lexeme
let lparen = ch '(' |> lineme
let rparen = ch ')' |> lexeme
let rangle = ch '>' |> lexeme
let equal  = ch '=' |> lineme
let plus   = ch '+' |> lexeme
let tilde  = ch '~' |> lexeme
let rarrow = str "=>" |> lineme
let dollar = ch '$' |> lexeme
let underscore = ch '_' |> lexeme
let ident  = id |> lexeme

let keyword (s:string) = ch '@' |> follow (str s) |> lexeme

let foldl1 f = function
  | (x :: xs) -> List.fold_left f x xs
  | _         -> raise (Failure "foldl1: empty")

let expr = declare ()
let binder = declare ()
let term = declare ()
let decl = declare ()

let tag = tagged |> map (fun x -> `Tag x)
let parens = lparen |> follow expr |> skip lines |> skip rparen
let var = ident |> map (fun i -> `Var i)
let lambda_bound_body = many1 binder |> skip rarrow |> backtracking |> seq expr |> sep_by lines
                        |> map (fun pairs -> `Lam pairs)
let lambda_auto_body = expr |> map (fun e -> `Autolam e)
let lambda_body = alt [ lambda_bound_body; lambda_auto_body ]

let lambda = lbrace |> follow lambda_body |> skip rbrace
let autovar = dollar |> result `Autovar
let integer = numeral |> map Elron.Unicode.string_of_uchars
                      |> map int_of_string
                      |> map (fun x -> `Int x)
let let_expr = many1 decl |> seq expr
             |> map (fun (ds, expr) -> `Let (ds, expr))
let lazy_term = tilde |> follow term |> map (fun e -> `Lazy e)

let () = alt [ var ; let_expr ; tag ; parens ; lambda ; integer ; autovar ; lazy_term ]
         |> implement term
let sequence = many1 term |> map (foldl1 (fun x xs -> `Apply (x, xs)))
let chain = sep_by rangle sequence |> map (foldl1 (fun x xs -> `Chain (x, xs)))

let () = implement expr chain

let binder_var = ident |> map (fun i -> `Var i)
let binder_paren = lparen |> follow binder |> skip rparen
let binder_tagged = tagged |> seq (many binder)
                  |> map (fun (name, binders) -> `Tagged (name, binders))

let () = implement binder (alt [ binder_tagged ; binder_paren ; binder_var ])

let normal_decl = plus |> follow ident |> seq (many binder) |> skip equal |> seq expr
                |> map (fun ((name, binders), body) -> `Func (name, binders, body))

let method_placeholder = alt [ ident |> map (fun x -> `Var x) ; underscore |> result (`Discriminant) ]
let method_decl = keyword "method" |> follow ident |> seq (many method_placeholder)
                |> map (fun (name, placeholders) -> `Method (name, placeholders))

let () = implement decl (alt [ normal_decl ; method_decl ] |> skip lines)

let module_ = many decl |> map (fun ds -> `Module ds)

let p = lines |> follow module_

let parse (stream : unit -> uchar_token) : t =
  Elron.Unicode.parse p stream
