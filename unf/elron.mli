type ('a, 't) parser

type parse_error = int * string list
exception ParseError of parse_error

val pure : 'a -> ('a, 't) parser
val fail : string list -> ('a, 't) parser

val backtracking : ('a, 't) parser -> ('a, 't) parser

val satisfy : ('t -> bool) -> ('t, 't) parser

val eq : 'a -> ('a, 'a) parser
val oneOf : 'a list -> ('a, 'a) parser
val exact : 't list -> ('t list, 't) parser


val eof : unit -> (unit, 'a) parser
val start : unit -> (unit, 'a) parser

val alt : ('a, 't) parser list -> ('a, 't) parser
val seq : ('a, 't) parser -> ('b, 't) parser -> ('b * 'a, 't) parser

val follow : ('a, 't) parser -> ('b, 't) parser -> ('a, 't) parser
val skip : ('a, 't) parser -> ('b, 't) parser -> ('b, 't) parser

val many : ('a, 't) parser -> ('a list, 't) parser
val many1 : ('a, 't) parser -> ('a list, 't) parser
val map : ('a -> 'b) -> ('a, 't) parser -> ('b, 't) parser

val result : 'a -> ('b, 't) parser -> ('a, 't) parser

val desc : string -> ('a, 't) parser -> ('a, 't) parser

(* helpers for forward declaration of recursive parsers *)
val declare : unit -> ('a, 't) parser
val implement : ('a, 't) parser -> ('a, 't) parser -> unit

type 't tokstate = Start | Tok of 't | Err of string | Eof
val parse : ('a, 't) parser
            -> ?formatter:('t -> string)
            -> (unit -> 't tokstate)
            -> 'a

val sep_by : ('s, 't) parser -> ('a, 't) parser -> ('a list, 't) parser

val default : 'a -> ('a, 't) parser -> ('a, 't) parser
val opt : ('a, 't) parser -> ('a option, 't) parser
val ignore : ('a, 't) parser -> (unit, 't) parser

module Unicode : sig
  open Batteries
  type uchar_token = UChar.t tokstate
  type 'a uchar_parser = ('a, UChar.t) parser

  val format : UChar.t -> string
  val str : string -> string uchar_parser
  val ch  : char   -> UChar.t uchar_parser
  val nch : char   -> UChar.t uchar_parser
  val noneOf : string -> UChar.t uchar_parser
  val parse : 'a uchar_parser -> (unit -> uchar_token) -> 'a
  val range : char -> char -> (UChar.t, UChar.t) parser
  val string_of_uchars : UChar.t list -> string
end
