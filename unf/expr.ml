open Batteries
open Batteries.Printf

type binder = [
  | `Var of string
  | `Tagged of string * (binder list)
]

let rec binder_to_string = function
  | `Var name -> printf "binder_to_string (`Var %s)\n" name ; name
  | `Tagged (name, bs) -> sprintf ".%s %s" name (bs |> List.map binder_to_string |> String.join " ")

type 'e a_decl = [
  | `Method of string * [ `Var of string | `Discriminant ] list
  | `Impl of string * binder list * 'e
  | `Func of string * binder list * 'e
]

type 'e a_syntax = [
  | `Autovar
  | `Var of string
  | `Tag of string
  | `Int of int
  | `Lam of (binder list * 'e) list
  | `Autolam of 'e
  | `Let of 'e a_decl list * 'e
  | `Lazy of 'e
  | `Apply of 'e * 'e
  | `Chain of 'e * 'e
]

let a_decl_to_string (formatter:'e -> string) : 'e a_decl -> string = function
  | `Method (name, _) -> sprintf "@method %s TODO" name
  | `Impl (name, _, e) -> sprintf "@impl %s TODO = %s" name (formatter e)
  | `Func (name, [], body) -> sprintf "+ %s = %s" name (formatter body)
  | `Func (name, bs, body) ->
      sprintf "+ %s %s = %s" name (bs |> List.map binder_to_string |> String.join(" "))
                                  (formatter body)

let a_decls_to_string formatter ds = ds |> List.map (a_decl_to_string formatter) |> String.join("; ")

let a_syntax_to_string (formatter:'e -> string) : 'e a_syntax -> string = function
  | `Var x -> x
  | `Autovar -> "$"
  | `Tag s -> "." ^ s
  | `Int i -> sprintf "%i" i
  | `Lam clauses ->
      let format_clause (binders, body) =
        sprintf "%s => %s;" (List.map binder_to_string binders |> String.join(" "))
                            (formatter body)
      in clauses |> List.map format_clause |> String.join(" ") |> sprintf "[ %s ]"
  | `Lazy e -> sprintf "~%s" (formatter e)
  | `Autolam e -> sprintf "[ %s ]" (formatter e)
  | `Let (ds, e) -> sprintf "%s; %s" (a_decls_to_string formatter ds) (formatter e)
  | `Apply (f, a) -> sprintf ("%s %s") (formatter f) (formatter a)
  | `Chain (a, f) -> sprintf "%s > %s" (formatter a) (formatter f)

type plain_syntax = plain_syntax a_syntax
type plain_decl = plain_syntax a_decl

let rec plain_syntax_to_string s = a_syntax_to_string plain_syntax_to_string s
let plain_decl_to_string d = a_decl_to_string plain_syntax_to_string d
let plain_decls_to_string ds = a_decls_to_string plain_syntax_to_string ds

type 'e a_expr = [
  | `Var of string
  | `Tag of string
  | `Int of int
  | `Case of 'e * (binder * 'e) list
  | `Unilam of string * 'e
  | `Lazy of 'e
  | `Let of 'e a_decl list * 'e
  | `Apply of 'e * 'e
]

let a_expr_to_string (formatter:'e -> string) (e : 'e a_expr) : string =
  printf "HERE"; match e with
  | `Var x -> x
  | `Tag s -> "." ^ s
  | `Int i -> sprintf "%i" i
  | `Case (disc, clauses) ->
      let s_disc = formatter disc
      in let s_clauses = clauses |> List.map (fun (b, e) ->
                                              sprintf "%s : %s"
                                                      (binder_to_string b)
                                                      (formatter e))
                                 |> String.join "; "
      in sprintf "@case %s { %s }" s_disc s_clauses
  | `Let (decls, e) ->
      sprintf "%s; %s" (a_decls_to_string formatter decls) (formatter e)
  | `Lazy e -> sprintf "~%s" (formatter e)
  | `Unilam (name, body) -> sprintf "[ %s => %s ]" name (formatter body)
  | `Apply (f, arg) -> sprintf "(%s %s)" (formatter f) (formatter arg)

type 'e expr_decl = 'e a_expr a_decl
type plain_expr = plain_expr a_expr
type plain_expr_decl = plain_expr expr_decl
let rec plain_expr_to_string e = a_expr_to_string plain_expr_to_string e


let map_subexprs (f : 'a -> 'b) : 'a a_expr -> 'b a_expr = function
  | `Var x -> `Var x
  | `Tag s -> `Tag s
  | `Int i -> `Int i
  | `Case (disc, clauses) ->
      let m_clauses = clauses |> List.map (fun (b, e) -> (b, f e))
      in `Case (f disc, m_clauses)
  | `Unilam (a, body) -> `Unilam (a, f body)
  | `Lazy e -> `Lazy (f e)
  | `Let (decls, body) ->
      let m_decl = function
        | `Method (n, bs) -> `Method (n, bs)
        | `Impl (n, bs, body) -> `Impl (n, bs, f body)
        | `Func (n, bs, body) -> `Func (n, bs, f body)

      in `Let (List.map m_decl decls, f body)
  | `Apply (fn, arg) -> `Apply (f fn, f arg)

let decl_subexprs : 'e a_decl -> 'e list = function
  | `Method (_, _) -> []
  | `Impl (_, _, b) -> [b]
  | `Func (_, _, b) -> [b]

let subexprs : 'e a_expr -> 'e list = function
  | `Case (disc, clauses) -> disc :: (clauses |> List.map (fun (_, e) -> e))
  | `Unilam (_, body) -> [body]
  | `Lazy e -> [e]
  | `Let (decls, body) ->
      body :: (decls |> List.map decl_subexprs |> List.flatten)
  | `Apply (f, a) -> [f; a]
  | _ -> []
