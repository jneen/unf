open Batteries
open Printf
open Expr
open Sugar
open Annotate

exception TypeError of string * string
exception VariableError of string
exception MatchError of string

type 'a impl = ('a list -> 'a)

type value =
  | INT of int
  | STR of string
  | FUNC of int * value impl
  | PARTIAL of int * value * value list
  | METHOD of string * int * (string, value impl) Map.t
  | TAGGED of string * (value list) (* NB: the values are reversed *)
  | ACTION of (unit -> value)
  | LAZY of string * value Lazy.t
  | MODULE of string * (string, value) Map.t
  | NATIVE of string * Obj.t

let rec string_of_value = function
  | INT i -> sprintf "%i" i
  | STR s -> s
  | FUNC (arity, _) ->
      sprintf "<func/%i>" arity
  | METHOD (name, arity, _) ->
      sprintf "<method %s/%i>" name arity
  | PARTIAL (remaining, callable, values) ->
      let underscores = List.make remaining "_" in
      let args = List.append (values |> List.map string_of_value) underscores
               |> String.join(" ")
      in sprintf "[ %s %s ]" (string_of_value callable) args
  | ACTION a -> "<action>"
  | TAGGED (t, vs) -> sprintf ".%s %s" t (vs |> List.map string_of_value |> List.rev |> String.join " ")
  | LAZY (s, lv) -> if Lazy.is_val lv
                    then sprintf "~!(%s)" (string_of_value (Lazy.force lv))
                    else sprintf "~(%s)" s
  | NATIVE (t, _) -> sprintf "<%s>" t
  | MODULE (name, bindings) ->
      let bindings_s = Map.bindings bindings |> List.map (fun (name, v) ->
                                                            sprintf " %s = %s "
                                                                    name
                                                                    (string_of_value v))
                                             |> String.join ";"
      in sprintf "@module %s [%s]" name bindings_s

type env = (string, value) Map.t

let rec bind : binder * value -> env option = function
  | `Var name, v -> Some (Map.singleton name v)
  | `Tagged (t, bs), TAGGED (t', vs) when t = t' && List.length bs = List.length vs
    -> let open List
       in combine bs vs |> fold_left (fun (env_opt:env option) (b, v) ->
                                          Option.bind env_opt (fun env ->
                                          (bind (b, v)) |> Option.map (fun env' ->
                                            Map.union env env'))) (Some Map.empty)
  | `Tagged (t, bs), LAZY (e, l) -> bind (`Tagged (t, bs), Lazy.force l)
  | _ -> None

let rec bind_select value = function
  | [] -> raise (MatchError "no match!")
  | (((b::_),expr)::bs) -> (match bind (b, value) with
               | Some env -> (env, expr)
               | None -> bind_select value bs)
  | _ -> raise (Failure "TODO: multiple binders")

let get_var name env =
  try Map.find name env
  with Not_found -> raise (VariableError (sprintf "no such variable %s" name))

let rec eval_expr env : plain_expr -> value = function
  | `Var name -> get_var name env
  | `Tag tag -> TAGGED (tag, [])
  | `Int i -> INT i
  | `Unilam (name, body) ->
      FUNC (1, (function [v] -> eval_expr (Map.add name v env) body | _ -> raise (Failure "oops")))
  | `Let (decls, body) ->
      raise (Failure "TODO: eval `Let")
  | `Lazy e -> LAZY (plain_expr_to_string e, lazy (eval_expr env e))
  | `Case (disc_expr, clauses) ->
      let disc = eval_expr env disc_expr
      in let rec select = function
        | [] -> raise (MatchError "no match!")
        | ((binder, body)::bs) -> match bind (binder, disc) with
                                  | Some env' -> eval_expr (Map.union env env') body
                                  | None      -> select bs

      in select clauses
  | `Apply (f, arg) -> eval_expr env arg |> apply env (eval_expr env f)
  (* | _ -> raise (Failure "TODO") *)

and resolve_partial = function
  | (0, FUNC (_, impl), args) -> impl args
  | (0, METHOD (_, binders, impls), args) -> raise (Failure "TODO: method dispatch")
  | (rem, callable, args) -> PARTIAL (rem, callable, args)

and apply env f arg = match f with
  | FUNC (arity, fn) ->
      resolve_partial (arity - 1, f, [arg])
  | PARTIAL (remaining, callable, values) ->
      resolve_partial (remaining - 1, callable, arg::values)
  | TAGGED (s, xs) -> TAGGED (s, arg::xs)
  | _ -> raise (TypeError ("%callable", string_of_value f))

let extend_method closure m binders expr = match m with
  | METHOD (name, arity, impls) ->
      (* let new_impl values =
        let bind_merge env pair = Map.union (bind pair) env
        in let new_env = List.fold_left bind_merge closure
        in eval_expr new_env expr *)
      raise (Failure "TODO: method impls")
  | _ -> raise (TypeError ("%method", string_of_value m))

let add_decl (env:env) (decl:plain_decl) : env = match decl with
  | `Func (name, [], expr) -> Map.add name (eval_expr env (desugar expr)) env
  | `Func (name, binders, expr) -> Map.add name (eval_expr env (desugar (`Lam [(binders, expr)]))) env
  | `Method (name, args) ->
      let method_ = METHOD (name, List.length args, Map.empty)
      in Map.add name method_ env
  | `Impl (name, binders, expr) ->
     (try let m = Map.find name env
          in Map.add name (extend_method env m binders expr) env
      with Not_found -> raise (VariableError name))
  (* | _ -> raise (Failure "TODO: add_decl cases") *)


let eval env = function
  | `Expr e -> let d = desugar e
               in printf "desugared to %s\n" (plain_expr_to_string d);
                  eval_expr env d
  | `Module ds ->
      let bindings = List.fold_left add_decl env ds
      in MODULE ("<top>", bindings)
  | _ -> raise (Failure "TODO: here")

module Conv = struct
  exception ArityError of int * int

  type 'a conv = value -> 'a

  let func1 (conv:'a conv) (f:'a conv) : value =
    FUNC (1, (function | [x] -> f (conv x)
                       | l -> raise (ArityError (1, List.length l))))

  let func2 ((c1:'a conv), (c2:'b conv)) (f: ('a * 'b) -> value) : value =
    FUNC (2, (function | [x; y] -> f (c1 x, c2 y)
                       | l -> raise (ArityError (2, List.length l))))

  let func3 ((c1:'a conv), (c2:'b conv), (c3:'c -> value)) (f: 'a * 'b * 'c -> value)
               : value =
    FUNC (3, (function | [x; y; z] -> f (c1 x, c2 y, c3 z)
                       | l -> raise (ArityError (3, List.length l))))

  let force : value -> value = function
    | LAZY (_, l) -> Lazy.force l
    | v -> v

  let t_int v = force v |> function
    | INT x -> x
    | v -> raise (TypeError ("%int", string_of_value v))
end

let prelude = let open Conv in
  Map.empty
  |> Map.add "add" (func2 (t_int, t_int) (fun ((x:int), (y:int)) -> INT (x + y)))
