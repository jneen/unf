PACKAGE_DIR = ./package
FINAL_BIN = $(PACKAGE_DIR)/unf
EXE_ARTIFACT = _build/unf/main.byte

.PHONY: all
all: $(FINAL_BIN)

.PHONY: $(FINAL_BIN)
$(FINAL_BIN): $(EXE_ARTIFACT)
	mkdir -p $(@D)
	cp $^ $@

.PHONY: $(EXE_ARTIFACT)
$(EXE_ARTIFACT): _tags setup.ml
	ocaml setup.ml -build

_tags:
	touch $@

setup.ml: _oasis
	oasis setup
